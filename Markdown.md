# Cours de Markdown
***
## C'est quoi le Markdown?  
- C'est un langage de prise de note et de mise en forme de texte.   
Son but est d'offrir une syntaxe facile à lire et à écrire.
## Comment stocker mon markdown avec gitlab?
- Créer un nouveau projet dans gitlab
- Mettre le projet en public et l'initialise
- Sélectionner l'adresse clone with HTTPS et le copier sur un nouveau projet sur Webstorm    
- Ajouter le fichier avec ***Add*** 
- Personaliser le nom du fichier dans la rubrique ***Commit*** *clt/K*    
- Transférer le fichier dans gitlab ***Push*** *clt/shift/k*   

### Les Backticks
- C'est un caractére appelé ***Backtick*** ou *apostrophe* inversée au début et à la fin du segment correspondant au code.   


## Comment structurer ma page 

### Les Titres
- Pour créer ***un Titre*** il faut rajouter *des signes numériques (#)*    
- Le nombre des ***(#)*** qu'on utilise doit correspondre au niveau ***des Titres***
### Les paragraphes 
- Pour séparer deux paragraphes il faut utiliser le sauts de ligne (terminer un ligne avec deux espaces ou plus puis taper return)  
### Gras et italique 

#### Gras 
- *** le texte ***  
- ___ le texte ___  
#### Italique 
- * le texte  *
- _ le texte _

### Les listes

#### listes ordonnées
Pour créer une liste ordonée, ajouter des nombres suivis de points, la liste doit commencer par le numéro un.   
 1. Premier article
 2. Deuxiéme article
 3. Troisiéme article      
 9. Quatriéme article   

#### Listes non ordonées   
 Pour créer une liste non ordonée, on ajoutes des tirets (-),des astérisques (*) ou des signes plus(+) devant la ligne.   
 - Premier article
 * Deuxiéme article   
 + Troisiéme article

## Commit
 - ctrl/k   
## Push
  