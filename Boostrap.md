# Cours de BOOSTRAP
![](BOOSTRAP.jpg) 
***

## C'est quoi le Boostrap?
- Boostrap est une boite d'outils open source pour le développement avec HTML, CSS et JS.   

## Les balises    
- <***section***>/<***section***>   
  ***Section*** permet de diviser le contenu du site en plusieurs catégories.   
- <***header***>/<***header***>   
   ***Header*** c'est le sous titre ou bien des paragraphes.   
- <***footer***>/<***footer***>  
   ***Footer*** qui permet de rajouter des informations supplémentaires.   
- <***div***>/<***div***>    
- <***span***>/<***span***>
   
## Les étapes à suivre   
- Il faut accéder sur le site:  https://getbootstrap.com/docs/4.3/getting-started/introduction/ 
- Puis vous aurez deux versions ***CSS*** et ***Javascript*** 
![](css%20et%20js.png) 
- Coller la balise dans le header
- Selectionner ***components*** et choisir selon votre besoin
### row et col
 ***row*** et ***col*** permettent de diviser la page en ligne ***row*** et en colonne ***col*** 
 Un ***row*** doit étre suivi par un ***col***